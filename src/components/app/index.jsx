 import React, { useState } from "react";
import { useSelector } from "react-redux"
import Items from "../Items";
import './style.scss';

const App =  ()=>{
  const [ isScroll, setScroll ] = useState(false);
  const { data } = useSelector(state=>state.albums);
  const handlerScroll = ()=>{
    setTimeout(()=>{
      setScroll(false)
    },100)
    setScroll(true);
  }
  const ownClass = isScroll? ' scrolling': '';
  return (
    <>
     <div className={'wrapper'  + ownClass} onScroll={()=>{
       handlerScroll();
     }}>
        <Items data= { data} />
     </div>
    </>
  )
}

export default App;
import React, {memo} from 'react';
import Item from '../Item';

const Items = ({data})=>{
  return(
    <>
     <div className="flex-scroll">
         {
           data.map((e,index )=>{
             const { isActive, value } = e;
              return(
                <Item 
                  name={value} 
                  key={(Math.random() + 1).toString(36).substring(7)}
                  isActive={isActive} 
                  index={index}
                />
              )
           })
         }
      </div>
    </>
  )
}

export default memo(Items);
import React  from "react";
import { useDispatch } from "react-redux";
import { changeActive } from "../../redux/reducer/albums";
import './style.scss';

const Item = ({name,isActive, index }) => {
  const dispatch = useDispatch();
  const ownClass = isActive? ' active':'';
  const handlerClick = () =>{
    dispatch((changeActive(index)))
  }
  return(
    <div className= { 'box' + ownClass } onClick={()=>{
      handlerClick();
    }}>{ name }</div>
  )
}

export default Item
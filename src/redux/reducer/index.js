import { combineReducers, configureStore,getDefaultMiddleware  } from "@reduxjs/toolkit"
import albums from "./albums";;

const rootReducer = combineReducers({
  albums
})
const store = configureStore({
  reducer:rootReducer,
  middleware:getDefaultMiddleware()
})

export default store;
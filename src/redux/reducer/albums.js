import { createSlice } from "@reduxjs/toolkit";


export const check = createSlice({
  name:"toolkit",
  initialState:{
   data : [
     {
       isActive:false,
       value:'Job Focus'
     },
     {
       isActive:false,
       value:'Soft Skills'
     },
     {
      isActive:false,
      value:'Technical Skills'
     },
     {
      isActive:false,
      value:'Functional Expertise'
     },
     {
      isActive:false,
      value:'Domain Expertise'
     },
     {
      isActive:false,
      value:'Patent Expertise'
     },
     {
      isActive:false,
      value:'Personal Expertise'
     }
   ]
  },
  reducers:{
    changeActive(state, {payload}){
      state.data.map(e=>e.isActive = false);
      state.data[payload].isActive = true;
    },
  }
})

export default check.reducer;

export const { changeActive } = check.actions;
